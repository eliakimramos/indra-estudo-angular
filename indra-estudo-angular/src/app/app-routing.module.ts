import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProdutosComponent } from './produtos.component';
import { HomeComponent } from './home.component';

const routes : Routes = [
    { path: '', component: HomeComponent },
    { path: 'produtos', component: ProdutosComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [RouterModule]
})

export class AppRoutingModule {}