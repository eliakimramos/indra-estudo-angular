import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProdutosComponent } from './produtos.component';
import { FormsModule } from '@angular/Forms';
import { ProdutoComponent } from './produto/produto.component';
import { ProdutosServicos } from './protudos.servicos';
import { HomeComponent } from './home.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    ProdutosComponent,
    ProdutoComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [ProdutosServicos],
  bootstrap: [AppComponent]
})
export class AppModule { }
