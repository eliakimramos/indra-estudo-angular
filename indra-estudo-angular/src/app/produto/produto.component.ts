import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ProdutosServicos } from '../protudos.servicos';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {
  @Input() nomeproduto: string;
  @Output() produtoClicado = new EventEmitter();
  constructor( private produtosServicos: ProdutosServicos) { }

  ngOnInit() {
  }

  onClicked(){
      // this.produtoClicado.emit();
      this.produtosServicos.delataProduto(this.nomeproduto);
  }
}
