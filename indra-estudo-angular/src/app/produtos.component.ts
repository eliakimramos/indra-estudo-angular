import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProdutosServicos } from './protudos.servicos';

@Component({
    selector: 'app-produtos',
    templateUrl: './produtos.component.html' 
})
export class ProdutosComponent implements OnInit, OnDestroy{
    produtosNome = "Livro de ficção";
    produtos = [];
    private produtosSubscription : Subscription;

    constructor (private produtosServicos:ProdutosServicos){
        
    }
    
    ngOnInit(){
        this.produtos = this.produtosServicos.getProtudos();
        this.produtosSubscription = this.produtosServicos.produtosAlterados.subscribe(() => {
            this.produtos = this.produtosServicos.getProtudos();
        });
    }

    ngOnDestroy(){
        this.produtosSubscription.unsubscribe();
    }

    onAddProduto(form){
        
        if(form.valid){
            // this.produtos.push(form.value.produtosNome);
            this.produtosServicos.addProduto(form.value.produtosNome);
        }
    }

    onRemoveProduto(produtoNome: string){
        this.produtos = this.produtos.filter(p => p !== produtoNome);
    }
}