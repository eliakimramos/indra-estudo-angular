
import { Subject } from "rxjs";

export class ProdutosServicos {
    private produtos = ['livro'];
    produtosAlterados = new Subject();

    addProduto(produtoNome:string){
        this.produtos.push(produtoNome);
        this.produtosAlterados.next();
    }

    getProtudos(){
        return [...this.produtos]
    }

    delataProduto(nomeproduto: string){
        this.produtos = this.produtos.filter(p => p !== nomeproduto);
        this.produtosAlterados.next();
    }
}